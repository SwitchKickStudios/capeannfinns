## SETUP

### Login to Heroku:

`heroku login`

### Check remotes:

`git remote -v`
`git remote`

### 1) Set up QA remote:

`heroku git:remote -a cape-ann-finns-qa`
`git remote rename heroku heroku-staging`

### 2) Set up PRODUCTION remote:

`heroku git:remote -a cape-ann-finns`

## RUN IT

`npm run start`

Will be available at localhost:3000

## DEPLOY

### Push to cape-ann-finns-qa:

`git push heroku-staging [name-of-your-branch]:master`

### Push to cape-ann-finns:

`git push heroku master`
module.exports.facebookGroupArray = [
    {
        name: 'American Finnish People',
        url: 'https://www.facebook.com/groups/180954511975450/'
    },
    {
        name: 'Finnish Culture & News Discussion',
        url: 'https://www.facebook.com/groups/1564318647140698/'
    },
    {
        name: 'Scandinavian Festivals',
        url: 'https://www.facebook.com/groups/scandinavianfestivals/about/'
    },
    {
        name: 'Proud to be Finnish',
        url: 'https://www.facebook.com/Proud-To-Be-Finnish-845104925602486/'
    },
    {
        name: 'Finnish Genealogy',
        url: 'https://www.facebook.com/groups/14108279806/'
    },
    {
        name: 'Finland in English: Expat Finland',
        url: 'https://www.facebook.com/expatfinland/'
    },
    {
        name: 'Finnish DNA MatchFinder',
        url: 'https://www.facebook.com/groups/FinnishDNA/'
    },
    {
        name: 'Very Finnish Problems',
        url: 'https://www.facebook.com/veryFinnishproblems/'
    },
    {
        name: 'Our Scandinavian & Finnish GENEALOGY',
        url: 'https://www.facebook.com/groups/1759419117666821/'
    },
    {
        name: 'Do I Look Finnish?',
        url: 'https://www.facebook.com/groups/1956813841239061/'
    },
    {
        name: 'Wood-Burning Sauna',
        url: 'https://www.facebook.com/groups/WoodBurningSauna/'
    },
    {
        name: 'Finnish Cooking & Culture',
        url: 'https://www.facebook.com/groups/FinnishCookingCulture/'
    },
    {
        name: 'Finnish American Heritage Center',
        url: 'https://www.facebook.com/Finnish-American-Heritage-Center-118220094925283/'
    },
    {
        name: 'The Finnish Book & Film Trove',
        url: 'https://www.facebook.com/groups/391021128036168/'
    },
    {
        name: 'Discovering Finland',
        url: 'https://www.facebook.com/DiscoveringFinland/'
    },
    {
        name: 'S I S U',
        url: 'https://www.facebook.com/InspireSisu/'
    },
    {
        name: 'The Finnish American Reporter',
        url: 'https://www.facebook.com/groups/130148453678788/'
    },
    {
        name: 'Cape Ann Finns',
        url: 'https://www.facebook.com/groups/1023145974511037/about/'
    },
    {
        name: 'Finlandia Foundation National',
        url: 'https://www.facebook.com/Finlandia.Foundation.National/'
    },
    {
        name: 'Embassy of Finland in the U.S.',
        url: 'https://www.facebook.com/FinnEmbassyDC/'
    },
    {
        name: 'Ink Tank',
        url: 'https://www.facebook.com/InkTankFinland/'
    },
    {
        name: 'Finns Are Awesome',
        url: 'https://www.facebook.com/groups/FinlandMyPride/'
    },
    {
        name: 'Concordia Finnish Language Villages',
        url: 'https://www.facebook.com/ConcordiaLanguageVillages/'
    },
    {
        name: 'Finland Today',
        url: 'https://www.facebook.com/finlandtoday/'
    }
];

const pathToGallery = './assets/gallery/';
module.exports.galleryArray = [
    {   // 2
        image: pathToGallery + 'immigrationAndEmmigrationMonuments.png',
        caption: 'Emigration Monument, Hanko, Finland, taking flight for North America',
        caption2: 'Immigration Monument, Bryant Park, Lake Worth, Florida, arriving in the U.S.'
    },
    {   // 3
        image: pathToGallery + 'rockportAndGloucester.png',
        caption: 'Rockport and Gloucester, Mass., home to 5000 Finns in early 20th century'
    },
    {   // 4
        image: pathToGallery + 'selmaKleimola.png',
        caption: ''
    },
    {   // 5
        image: pathToGallery + 'alexandraVarsamakiSeppala.png',
        caption: 'Alexandra Varsamaki Seppala, right, and friends mending loomed riepumatto or rag rugs'
    },
    {   // 6
        image: pathToGallery + 'traditionalSavuasauna.png',
        caption: 'Traditional sauvasauna (smoke sauna) of the Andrew Pistenmaa family, Lanesville, Gloucester, Mass.'
    },
    {   // 7
        image: pathToGallery + 'pelastajaTemporanceSociety2.jpg',
        caption: 'Pelastaja Temperance Society, Wainola hall, Waino band members and families, Lanesville, Mass.'
    },
    {   // 8
        image: pathToGallery + 'temporanceSocietyMembers.png',
        caption: 'Valon Leimu (Glow of Light) Temperance Society members, Rockport, Mass.'
    },
    {   // 9
        image: pathToGallery + 'finnishWorkersAssociationFinnishHall.png',
        caption: 'The Finnish Workers Association’s Finn Hall, Lanesville, Gloucester, Mass.'
    },
    {   // 10
        image: pathToGallery + 'murtajaSocialistWorkersHall.png',
        caption: 'Murtaja Socialist/workers hall, Squam Road, Rockport, Mass.'
    },
    {   // 11
        image: pathToGallery + 'finnishChurchWomensClub.png',
        caption: 'Finnish church (St. Paul Lutheran, Rockport, Mass.) Women’s Club'
    },
    {   // 12
        image: pathToGallery + 'americanLegionBand.png',
        caption: 'American Legion Band, Rockport, Mass.'
    },
    {   // 13
        image: pathToGallery + 'theFinnsMainStreet.png',
        caption: 'The Finns’ Main Street , Finn Alley (Forest St., Rockport, Mass.) St. Paul Lutheran church on left, Valon Leimu far right, “Rentalas” or apartments, near right, for bachelor Finnish quarry workers'
    },
    {   // 14
        image: pathToGallery + 'quarrymen.png',
        caption: 'Finnish men dominated the qranite quarry labor force on Cape Ann.'
    }
];

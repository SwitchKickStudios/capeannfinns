var express = require('express');
var app = express();
const path = require('path');
const router = express.Router();
const { fullpageJSLicense } = require('./config');
const galleryArray = require('./constants/galleryArray.js');
const facebookGroupArray = require('./constants/facebookGroupArray.js');

router.get('/',function(req,res){
    res.render(path.join(__dirname+'/index'), {
        galleryArray: galleryArray.galleryArray,
        facebookGroupArray: facebookGroupArray.facebookGroupArray,
        fullpageJSLicense
    });     //__dirname : It will resolve to your project folder.
});
app.use('/favicon.ico',express.static(path.join(__dirname+'/favicon.ico')));
app.use('/assets',express.static(path.join(__dirname+'/assets')));
app.use('/dist',express.static(path.join(__dirname+'/dist')));
app.use('/src',express.static(path.join(__dirname+'/src')));
app.set('view engine', 'ejs');

app.use('/', router);
app.listen(process.env.PORT || 3000);
